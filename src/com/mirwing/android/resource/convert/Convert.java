package com.mirwing.android.resource.convert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class Convert {
    private File _srcFolder, _outFolder;

    private static final double[] DENSITIES = {120, 160, 240, 213, 320, 480, 640};
    private static final String[] RESOL = {"ldpi", "mdpi", "hdpi", "tvdpi", "xhdpi", "xxhdpi", "xxxhdpi"};

    private static int START = 1;
    private static int MAX = 6;

    private int mBase;
    private int mTarget;

    public Convert(String srcName, String outName, int base, int target) {
        _srcFolder = new File(srcName);
        _outFolder = new File(outName);
        mBase = base;
        mTarget = target;
    }

    private void _ready() {
        String outName = _outFolder.getName();
        if (_outFolder.exists()) {
            File[] outFiles = _outFolder.listFiles();
            for (int i = 0; i < outFiles.length; i++) {
                outFiles[i].delete();
            }
            System.out.println(outName + " delete: " + (_outFolder.delete() ? "SUCCESS" : "FAILURE"));
        }
        System.out.println(outName + " mkdir: " + (_outFolder.mkdir() ? "SUCCESS" : "FAILURE"));
    }

    public void go() throws IOException {
        _ready();

        File[] srcFiles = _srcFolder.listFiles();
        File outFile = null;

        for (int i = 0; i < srcFiles.length; i++) {
            if (_check(new BufferedReader(new InputStreamReader(new FileInputStream(srcFiles[i]), "UTF-8"))))
            {
                System.out.println((i+1) + "\t: " + srcFiles[i].getName());
                outFile = new File(_outFolder, srcFiles[i].getName());
                System.out.println("\t> " + outFile.getAbsolutePath() + " createNewFile: " + (outFile.createNewFile() ? "SUCCESS" : "FAILURE"));

                _modify(new BufferedReader(new InputStreamReader(new FileInputStream(srcFiles[i]), "UTF-8")),
                        new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8"))
                );
            }
        }
    }

    private boolean _check(BufferedReader br) throws IOException {
        String line;
        while ((line = br.readLine()) != null) {
            if (_checkLine(line, "dip")
                    || _checkLine(line, "dp")
                    || _checkLine(line, "sp")) {
                return true;
            }
        }
        return false;
    }

    private boolean _checkLine(String line, String keyword) {
        boolean retValue = false;
        String[] parts = line.split(keyword);
        if (parts.length > 1) {
//			int p = parts[0].lastIndexOf('\"');
//			if (p != -1 && parts[1].startsWith("\"")) {
            int p = parts[0].lastIndexOf('>');
            if (p != -1 && parts[1].startsWith("<")) {
                try {
                    Double.parseDouble(parts[0].substring(p + 1));
                    retValue = true;
                } catch (NumberFormatException e) {
                }
            }
        }

        return retValue;
    }

    private void _modify(BufferedReader br, BufferedWriter bw) throws IOException {
        String line = null;
        while ((line = br.readLine()) != null) {
            line = _modifyLine(line, "dip");
            line = _modifyLine(line, "dp");
            line = _modifyLine(line, "sp");

            bw.write(line);
            bw.write('\n');
            bw.flush();
        }
    }

    private String _modifyLine(String line, String keyword) {
        String[] parts = line.split(keyword);
        if (parts.length > 1) {
//			int p = parts[0].lastIndexOf('\"');
//			if (p != -1 && parts[1].startsWith("\"")) {
            int p = parts[0].lastIndexOf('>');
            if (p != -1 && parts[1].startsWith("<")) {
                try {
                    String head = parts[0].substring(0, p + 1);
                    double data = Double.parseDouble(parts[0].substring(p + 1));
                    double round = (DENSITIES[mBase] / 160) * (DENSITIES[mBase] / DENSITIES[mTarget]);
                    double edit = Math.round(data * round * 100);
                    String value = String.valueOf(edit/100);
                    if (value.startsWith(".0", value.length()-2))
                        value = value.substring(0, value.length()-2);
                    line = head + value + keyword + parts[1];
                } catch (NumberFormatException e) {

                }
            }
        }

        return line;
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("java -jar jarfile argument1");
            System.out.println("argument1 : baseline(ldpi ~ xxxdpi)");
            return;
        }

        for (int i=0; i<DENSITIES.length; ++i) {
            if (args[0].equals(RESOL[i])) {
                START = i;
                break;
            }
        }

        for (int i=0; i<RESOL.length; ++i) {
            if (args.length <= 1) {
                MAX = RESOL.length - 1;
                break;
            } else if (args[1].equals(RESOL[i])) {
                MAX = i;
                break;
            }
        }

        try {
            String base = "values";
            if (!"mdpi".equals(RESOL[START])) {
                base = base.concat("-");
                base = base.concat(RESOL[START]);
            }

            for (int i=0; i<=MAX; ++i) {
                String target = "values";
                target = target.concat("-");
                target = target.concat(RESOL[i]);
                new Convert(base, target, START, i).go();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
